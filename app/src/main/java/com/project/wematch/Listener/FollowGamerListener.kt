package com.project.wematch.Listener

/**
 * Created by fish on 24/11/2017.
 */
open interface FollowGamerListener {
    fun onFollowGamer(gamerId: String)
}