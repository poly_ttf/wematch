package com.project.wematch.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.wematch.TO.MetaTO


/**
 * Created by Fish on 2017/11/22.
 */

abstract class BaseItemListAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    val context: Context

    val ITEMS_PER_PAGE = 10

    private val TYPE_ITEM: Int = 0
    private val TYPE_LOAD_MORE: Int = 2
    private val TYPE_EMPTY: Int = 3

    private val listMeta: MutableList<MetaTO> = mutableListOf()
    protected var mCallback: Callback? = null
    open var mFilterCallback: FilterCallback<T>? = null
    private var itemsPerRow = 1
    private var layoutMode: LayoutMode? = LayoutMode.LIST
    private var result: MutableList<T> = mutableListOf()
    private var resultOri: MutableList<T> = mutableListOf()
    private var viewCount: Int = 0
    private var offset: Int = 0
    private var flagLoading = false
    private var enableSearch = false

    constructor(context: Context){
        this.context = context
    }

    enum class LayoutMode {
        LIST, GRID
    }

    fun restoreOriList() {
        setFilterResult(resultOri)
    }

    fun setFilterResult(result: List<T>) {
        this.result.clear()
        this.result.addAll(result)
        updateMeta(0)
    }

    private fun setResult(result: List<T>) {
        this.result.clear()
        this.result.addAll(result)
        this.resultOri.addAll(result)
        updateMeta(0)
    }

    fun setResult(result: List<T>, offset: Int) {
        this.offset = offset
        setResult(result)
    }

    fun insertResult(result: List<T>) {
        val prevSize = this.result.size
        this.result.addAll(result)
        this.resultOri.addAll(result)
        if (listMeta[viewCount - 1].viewType === TYPE_LOAD_MORE) {
            listMeta.removeAt(viewCount - 1)
        }
        updateMeta(prevSize)
    }

    protected fun updateMeta(prevSize: Int) {
        var i = prevSize
        if (i <= 0) {
            listMeta.clear()
        }
        val n = this.result.size
        while (i < n) {
            listMeta.add(MetaTO(TYPE_ITEM, i))
            i++
        }
        viewCount = listMeta.size
        flagLoading = false
        notifyDataSetChanged()
    }

    fun notifyError() {
        if (flagLoading) {
            offset -= 10
        }
        this.flagLoading = false
        notifyDataSetChanged()
    }

    protected abstract fun createItemHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder

    protected abstract fun renderItemView(holder: RecyclerView.ViewHolder, data: T, position: Int)

    override fun getItemViewType(position: Int): Int {
        return listMeta[position].viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            TYPE_ITEM -> return createItemHolder(inflater, parent)
        }
        return null
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val meta = listMeta[position]
        when (meta.viewType) {
            TYPE_ITEM -> renderItemView(holder, result[meta.typeIndex], position)
        }
    }

    override fun getItemCount(): Int {
        return viewCount
    }

    private fun setFullWidthLayoutParams(itemView: View) {
        setFullWidthLayoutParams(itemView, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setFullWidthLayoutParams(itemView: View, height: Int) {
        if (layoutMode == LayoutMode.GRID) {
            val layoutParams = StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height)
            layoutParams.isFullSpan = true
            itemView.layoutParams = layoutParams
        }
    }

    interface Callback {
        fun onLoadMoreResult(offset: Int)

        fun onItemSelected(item: Any, holder: RecyclerView.ViewHolder)
    }

    fun filter(str: String) {
        mFilterCallback?.filter(str, resultOri)
    }

    interface FilterCallback<T> {
        fun filter(str: String, result: MutableList<T>)
    }

    fun setCallback(callback: Callback) {
        this.mCallback = callback
    }

    fun setFilterCallback(callback: FilterCallback<T>) {
        this.mFilterCallback = callback
    }

}
