package com.project.wematch.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.project.wematch.R
import com.project.wematch.TO.RoomHistoryTO
import com.project.wematch.Util.ViewUtils
import com.squareup.picasso.Picasso

/**
 * Created by frankiefong on 24/11/2017.
 */

class GameHistoryAdapter : BaseItemListAdapter<RoomHistoryTO> {

    private var metrics: DisplayMetrics? = null

    constructor(context: Context) : super(context) {
        metrics = ViewUtils.getDisplayMetrics(context)
    }

    override fun createItemHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return RoomHolder(inflater.inflate(R.layout.holder_room_history, parent, false))
    }

    override fun renderItemView(holder: RecyclerView.ViewHolder, data: RoomHistoryTO, position: Int) {
        (holder as RoomHolder).render(data)
    }

    private inner class RoomHolder : RecyclerView.ViewHolder, View.OnClickListener {

        private val ivGamePic: ImageView

        var room: RoomHistoryTO? = null

        constructor(itemView: View) : super(itemView) {
            ivGamePic = itemView as ImageView

            itemView.layoutParams.width = metrics!!.widthPixels / 3
            itemView.setOnClickListener(this)
        }

        fun render(room: RoomHistoryTO) {
            this.room = room

            Picasso.with(context).load(room.image).into(ivGamePic)

        }

        override fun onClick(p0: View?) {
            if (mCallback != null) {
                mCallback?.onItemSelected(room as Any, this)
            }
        }

    }

}