package com.project.wematch.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.joanzapata.iconify.widget.IconTextView
import com.project.wematch.Listener.FollowGamerListener
import com.project.wematch.R
import com.project.wematch.TO.RoomDetailTO
import com.project.wematch.TO.RoomTO
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.holder_room_listing.view.*

/**
 * Created by Fish on 2017/11/22.
 */

class HomeAdapter(context: Context) : BaseItemListAdapter<RoomDetailTO>(context = context) {

    private var roomListCallback: RoomListingCallback? = null

    interface RoomListingCallback {
        fun onJoinRoomClick(item: Any, holder: RecyclerView.ViewHolder)

        fun onGamerInfoClick(item: Any, holder: RecyclerView.ViewHolder)

        fun onModeClicked(mode: String)
    }

    fun setFollowGamerCallback(callback: RoomListingCallback) {
        this.roomListCallback = callback
    }

    override fun createItemHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return RoomViewHolder(inflater.inflate(R.layout.holder_room_listing, parent, false))
    }

    override fun renderItemView(holder: RecyclerView.ViewHolder, data: RoomDetailTO, position: Int) {
        if (holder is RoomViewHolder) {
            holder.render(data, position)
        }
    }

    private inner class RoomViewHolder : RecyclerView.ViewHolder, View.OnClickListener {

        val tvGameName: TextView
        val tvGameDate: TextView
        val tvJoints: TextView
        val ivJoinRoom: IconTextView
        val ivUserPic: ImageView
        val ivGamePic: ImageView
        val llMode: LinearLayout
        val btnGameModeRegular: Button
        val btnGameModeQuick: Button

        var room: RoomDetailTO? = null

        constructor(itemView: View) : super(itemView) {
            tvGameName = itemView.findViewById(R.id.tv_game_name)
            tvGameDate = itemView.findViewById(R.id.tv_game_date)
            tvJoints = itemView.findViewById(R.id.tv_joints)
            ivJoinRoom = itemView.findViewById(R.id.iv_join_room)
            ivUserPic = itemView.findViewById(R.id.iv_user_pic)
            ivGamePic = itemView.findViewById(R.id.iv_game_pic)
            llMode = itemView.findViewById(R.id.ll_mode)
            btnGameModeRegular = itemView.findViewById(R.id.btnGameModeRegular)
            btnGameModeQuick = itemView.findViewById(R.id.btnGameModeQuick)

            itemView.setOnClickListener(this)
            ivJoinRoom.setOnClickListener(this)
            ivUserPic.setOnClickListener(this)
            btnGameModeRegular.setOnClickListener(this)
            btnGameModeQuick.setOnClickListener(this)
        }

        fun render(room: RoomDetailTO, position: Int) {
            this.room = room

            tvGameName.text = room.name
            tvGameDate.text = room.game_date + " " + room.game_time
            tvJoints.text = room.total_join + " Game Players"
            if (room.is_join) {
                ivJoinRoom.setText(" {fa-heart} Joint")
            } else {
                ivJoinRoom.setText(" {fa-heart-o} Join Now")
            }

            if (adapterPosition == 0) {
                llMode.visibility = View.VISIBLE
            } else {
                llMode.visibility = View.GONE
            }

            Picasso.with(context).load(room.profile_pic).placeholder(R.drawable.ic_launcher).into(ivUserPic)
            Picasso.with(context).load(room.image).into(ivGamePic)
        }

        override fun onClick(v: View?) {
            if (v == itemView) {
                mCallback?.onItemSelected(room as Any, this@RoomViewHolder)
            }
            if (v == ivJoinRoom) {
                roomListCallback?.onJoinRoomClick(room as Any, this@RoomViewHolder)
            }
            if (v == ivUserPic) {
                roomListCallback?.onGamerInfoClick(room as Any, this@RoomViewHolder)
            }
            if (v == btnGameModeRegular) {
                btnGameModeRegular.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackgroundSelected))
                btnGameModeRegular.setTextColor(Color.parseColor("#FFFFFF"))
                btnGameModeQuick.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground))
                btnGameModeQuick.setTextColor(Color.parseColor("#000000"))
                roomListCallback?.onModeClicked("0")
            }
            if (v == btnGameModeQuick) {
                btnGameModeQuick.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackgroundSelected))
                btnGameModeQuick.setTextColor(Color.parseColor("#FFFFFF"))
                btnGameModeRegular.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground))
                btnGameModeRegular.setTextColor(Color.parseColor("#000000"))
                roomListCallback?.onModeClicked("1")
            }
        }

    }

}