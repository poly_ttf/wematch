package com.project.wematch.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.project.wematch.TO.GameTO
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.project.wematch.R
import com.project.wematch.Util.ViewUtils
import com.squareup.picasso.Picasso

/**
 * Created by frankiefong on 24/11/2017.
 */

class SelectGameAdapter : BaseItemListAdapter<GameTO> {

    private var metrics: DisplayMetrics? = null

    constructor(context: Context) : super(context) {
        metrics = ViewUtils.getDisplayMetrics(context)

        setFilterCallback(object : FilterCallback<GameTO> {
            override fun filter(str: String, result: MutableList<GameTO>) {
                if (str != "") {
                    val result: List<GameTO> = result.filter { it.name?.contains(str, true) ?: false }
                    setFilterResult(result)
                } else {
                    restoreOriList()
                }
            }

        })
    }

    override fun createItemHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return SelectGameHolder(inflater.inflate(R.layout.holder_select_game, parent, false))
    }

    override fun renderItemView(holder: RecyclerView.ViewHolder, data: GameTO, position: Int) {
        (holder as SelectGameHolder).render(data)
    }

    private inner class SelectGameHolder : RecyclerView.ViewHolder, View.OnClickListener {

        private val ivGamePic: ImageView
        private val tvGameName: TextView

        var game: GameTO? = null

        constructor(itemView: View) : super(itemView) {
            ivGamePic = itemView.findViewById(R.id.iv_game_pic)
            tvGameName = itemView.findViewById(R.id.tv_game_name)

            itemView.layoutParams.width = metrics!!.widthPixels / 2
            itemView.setOnClickListener(this)
        }

        fun render(game: GameTO) {
            this.game = game

            tvGameName.text = game.name
            Picasso.with(context).load(game.image).into(ivGamePic)
        }

        override fun onClick(p0: View?) {
            if (mCallback != null) {
                mCallback?.onItemSelected(game as Any, this)
            }
        }

    }

}