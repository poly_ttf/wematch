package com.project.wematch.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.joanzapata.iconify.widget.IconTextView
import com.project.wematch.R
import com.project.wematch.TO.GamerTO
import com.project.wematch.TO.RoomDetailTO
import com.squareup.picasso.Picasso

/**
 * Created by Fish on 2017/11/22.
 */

class GamerListAdapter(context: Context) : BaseItemListAdapter<GamerTO>(context = context) {

    override fun createItemHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        return GamerHolder(inflater.inflate(R.layout.holder_gamer, parent, false))
    }

    override fun renderItemView(holder: RecyclerView.ViewHolder, data: GamerTO, position: Int) {
        if (holder is GamerHolder) {
            holder.render(data, position)
        }
    }

    private inner class GamerHolder : RecyclerView.ViewHolder {

        val tvGamerName: TextView
        val ivUserPic: ImageView

        var gamer: GamerTO? = null

        constructor(itemView: View) : super(itemView) {
            tvGamerName = itemView.findViewById(R.id.tv_gamer_name)
            ivUserPic = itemView.findViewById(R.id.iv_gamer_pic)
        }

        fun render(gamer: GamerTO, position: Int) {
            this.gamer = gamer

            tvGamerName.text = gamer.username
            Picasso.with(context).load(gamer.profile_pic).placeholder(R.drawable.ic_launcher).into(ivUserPic)
        }
    }

}