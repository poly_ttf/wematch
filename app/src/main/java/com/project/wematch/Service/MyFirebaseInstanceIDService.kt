package com.project.wematch.Service

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.iid.FirebaseInstanceId
import com.project.wematch.Util.SharedPrefsUtils


/**
 * Created by fish on 10/11/2017.
 */
class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        if (refreshedToken != null) {
            SharedPrefsUtils.setStringPreference(this, "pushId", refreshedToken)
        }
        Log.d("Firebase", "RefreshedToken : " + refreshedToken)
    }
}