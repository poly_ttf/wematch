package com.project.wematch.Service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


/**
 * Created by fish on 10/11/2017.
 */
/**
 * Created by fish on 10/11/2017.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.d("FCM", "onMessageReceived:" + remoteMessage!!.from!!)
    }
}