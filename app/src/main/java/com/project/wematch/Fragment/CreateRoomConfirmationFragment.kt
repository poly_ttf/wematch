package com.project.wematch.Fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.project.wematch.Activity.MainActivity
import com.project.wematch.Activity.RoomDetailActivity
import com.project.wematch.DAO.RoomDAO
import com.project.wematch.R
import com.project.wematch.TO.CreateRoomTO
import com.project.wematch.TO.RoomTO
import com.project.wematch.Util.inflate
import com.project.wematch.Util.swapFragment
import com.project.wematch.Util.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import rx.Observable
import rx.Subscriber
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class CreateRoomConfirmationFragment : Fragment() {

    var v: View? = null

    val ivGamePic: ImageView? by lazy { v?.findViewById<ImageView>(R.id.iv_game_pic) }
    val tvGameName: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_game_name) }
    val tvGamePlatform: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_game_platform) }
    val tvGameType: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_game_type) }
    val tvRoomName: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_room_name) }
    val tvRoomCaption: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_room_caption) }
    val btnCreateRoom: Button? by lazy { v?.findViewById<Button>(R.id.btn_create_room) }

    private var gameSubscriber: Subscriber<RoomTO>? = null

    var createRoom: CreateRoomTO? = null

    companion object {
        fun newInstance(createRoom: CreateRoomTO): CreateRoomConfirmationFragment {
            val args = Bundle()
            args.putSerializable("createRoom", createRoom)

            val fragment: CreateRoomConfirmationFragment = newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): CreateRoomConfirmationFragment {
            return CreateRoomConfirmationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = container?.inflate(R.layout.fragment_create_room_confirmation)

            createRoom = if (arguments?.containsKey("createRoom") == true) {
                arguments?.getSerializable("createRoom") as CreateRoomTO
            } else {
                null
            }

            initView()
        }
        return v
    }

    fun initView() {
        Picasso.with(context).load(createRoom?.game?.image).into(ivGamePic)
        tvGameName?.text = createRoom?.game?.name
        tvGamePlatform?.text = createRoom?.gamePlatform?.name
        tvGameType?.text = createRoom?.gameType?.name
        tvRoomName?.text = createRoom?.room?.name
        tvRoomCaption?.text = createRoom?.room?.caption

        btnCreateRoom?.setOnClickListener { createRoom() }
    }

    fun createRoom() {
        if (createRoom!!.room!!.mode == "1") {
            //Quick Game
            val currentCalendar: Calendar = Calendar.getInstance()
            createRoom!!.room!!.game_date = currentCalendar.get(Calendar.DAY_OF_MONTH).toString() + "/" +
                    currentCalendar.get(Calendar.MONTH).toString() + "/" +
                    currentCalendar.get(Calendar.YEAR).toString()

            var hour = currentCalendar.get(Calendar.HOUR_OF_DAY)
            var amPm: String
            if (hour < 12) {
                amPm = "AM"
            } else {
                hour -= 12
                amPm = "PM"
            }
            createRoom!!.room!!.game_time = hour.toString() + ":" + currentCalendar.get(Calendar.MINUTE) + amPm
        }

        val gameTask: Observable<RoomTO>? = RoomDAO(activity).createRoom(createRoom?.room!!)
        gameSubscriber = object : Subscriber<RoomTO>() {
            override fun onNext(t: RoomTO) {
                activity.toast("Create room successful!")

                (activity as AppCompatActivity).swapFragment(HomeFragment.newInstance(), "")
                (activity as MainActivity).tl_main.getTabAt(0)?.select()

                val intent = Intent(activity, RoomDetailActivity::class.java)
                intent.putExtra("roomId", t.id)
                startActivity(intent)
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }

        }

        gameTask?.subscribe(gameSubscriber)
    }

}// Required empty public constructor
