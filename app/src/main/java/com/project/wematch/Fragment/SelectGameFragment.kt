package com.project.wematch.Fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.project.wematch.DAO.GameDAO

import com.project.wematch.R
import com.project.wematch.Util.inflate
import rx.Observable
import rx.Subscriber
import android.support.v7.widget.StaggeredGridLayoutManager
import android.widget.EditText
import com.project.wematch.Adapter.SelectGameAdapter
import android.text.Editable
import com.project.wematch.Activity.MainActivity
import android.text.TextWatcher
import com.project.wematch.Adapter.BaseItemListAdapter
import com.project.wematch.TO.*
import com.project.wematch.Util.swapFragment
import com.project.wematch.Util.toast
import kotlinx.android.synthetic.main.activity_main.*


/**
 * A simple [Fragment] subclass.
 */
class SelectGameFragment : Fragment() {

    var v: View? = null

    private val rvGames: RecyclerView? by lazy { v?.findViewById<RecyclerView>(R.id.rv_games) }
    val adapter: SelectGameAdapter? by lazy { SelectGameAdapter(activity) }
    private val etSearchGame: EditText? by lazy { v?.findViewById<EditText>(R.id.et_search_game) }

    private var gameSubscriber: Subscriber<List<GameTO>>? = null

    var createRoom: CreateRoomTO? = null
    var game: GameTO? = null

    companion object {
        fun newInstance(game: GameTO): SelectGameFragment {
            val args = Bundle()
            args.putSerializable("game", game)

            val fragment: SelectGameFragment = newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(createRoom: CreateRoomTO): SelectGameFragment {
            val args = Bundle()
            args.putSerializable("createRoom", createRoom)

            val fragment: SelectGameFragment = newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): SelectGameFragment {
            return SelectGameFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = container?.inflate(R.layout.fragment_select_game)

            createRoom = if (arguments?.containsKey("createRoom") == true) {
                arguments?.getSerializable("createRoom") as CreateRoomTO
            } else {
                null
            }

            game = if (arguments?.containsKey("game") == true) {
                arguments?.getSerializable("game") as GameTO
            } else {
                null
            }

            initView()
            searchGame()
        }
        return v
    }

    private fun initView() {
        rvGames?.layoutManager = StaggeredGridLayoutManager(2, 1)
        rvGames?.adapter = adapter
        adapter?.setCallback(object : BaseItemListAdapter.Callback {
            override fun onLoadMoreResult(offset: Int) {

            }

            override fun onItemSelected(item: Any, holder: RecyclerView.ViewHolder) {
                if (createRoom != null) {
                    createRoom?.game = item as GameTO
                    createRoom?.room?.game_id = item.id
                    (activity as AppCompatActivity).swapFragment(CreateRoomConfirmationFragment.newInstance(createRoom!!), "")
                } else if (game != null) {
                    (activity as AppCompatActivity).swapFragment(HomeFragment.newInstance(item as GameTO))
                }
            }

        })

        etSearchGame?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(cs: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
                // When user changed the Text
                adapter?.filter(cs.toString())
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int,
                                           arg3: Int) {
                // TODO Auto-generated method stub

            }

            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
            }
        })
    }

    private fun searchGame() {
        var gameTask: Observable<List<GameTO>>? = null
        if (createRoom != null) {
            gameTask = GameDAO(activity).searchGameByTypePlatform(createRoom?.gameType?.id ?: "0", createRoom?.gamePlatform?.id ?: "0")
        } else {
            gameTask = GameDAO(activity).searchGameByTypePlatform(game?.game_type_id!!, game?.game_platform_id!!)
        }
        gameSubscriber = object : Subscriber<List<GameTO>>() {
            override fun onNext(t: List<GameTO>?) {
                adapter?.setResult(t!!, t.size)

            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }

        }

        gameTask.subscribe(gameSubscriber)
    }

}// Required empty public constructor
