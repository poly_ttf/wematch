package com.project.wematch.Fragment


import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.project.wematch.Activity.MainActivity
import com.project.wematch.R
import com.project.wematch.TO.CreateRoomTO
import com.project.wematch.TO.RoomTO
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.StringUtils
import com.project.wematch.Util.inflate
import com.project.wematch.Util.swapFragment


/**
 * A simple [Fragment] subclass.
 */
class CreateRoomFragment : Fragment() {

    var v: View? = null

    val btnSubmit: Button? by lazy { v?.findViewById<Button>(R.id.btn_submit) }

    val etRoomName: EditText? by lazy { v?.findViewById<EditText>(R.id.et_room_name) }
    val etRoomCaption: EditText? by lazy { v?.findViewById<EditText>(R.id.et_room_caption) }
    val btnGameModeRegular: Button? by lazy { v?.findViewById<Button>(R.id.btnGameModeRegular) }
    val btnGameModeQuick: Button? by lazy { v?.findViewById<Button>(R.id.btnGameModeQuick) }
    val dpGameDate: DatePicker? by lazy { v?.findViewById<DatePicker>(R.id.dp_game_date) }
    val tpGameTime: TimePicker? by lazy { v?.findViewById<TimePicker>(R.id.tp_game_time) }
    val llRegular: LinearLayout? by lazy { v?.findViewById<LinearLayout>(R.id.ll_regular) }

    var mode = 0

    val createRoom: CreateRoomTO = CreateRoomTO()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = container?.inflate(R.layout.fragment_create_room)
            initView()
        }

        return v
    }

    fun initView() {
        btnGameModeRegular?.setOnClickListener({
            mode = 0
            it.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackgroundSelected))
            (it as Button).setTextColor(Color.parseColor("#FFFFFF"))
            btnGameModeQuick?.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground))
            btnGameModeQuick?.setTextColor(Color.parseColor("#000000"))
            llRegular?.visibility = View.VISIBLE
        })

        btnGameModeQuick?.setOnClickListener({
            mode = 1
            it.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackgroundSelected))
            (it as Button).setTextColor(Color.parseColor("#FFFFFF"))
            btnGameModeRegular?.setBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground))
            btnGameModeRegular?.setTextColor(Color.parseColor("#000000"))
            llRegular?.visibility = View.GONE
        })

        btnSubmit?.setOnClickListener {
            val room = RoomTO()
            room.name = etRoomName?.text.toString()
            room.caption = etRoomCaption?.text.toString()
            room.user_id = SharedPrefsUtils.getMyId(activity)
            room.game_date = "29/11/2017"
            room.game_time = "7:00PM"
            room.mode = mode.toString()
            room.game_date = getDate()
            room.game_time = getTime()

            createRoom.room = room

            (activity as AppCompatActivity).swapFragment(SearchGameFragment.newInstance(createRoom), "")
        }
    }

    fun getDate(): String = dpGameDate?.dayOfMonth.toString() + "/" + dpGameDate?.month.toString() + "/" + dpGameDate?.year.toString()

    fun getTime(): String {
        val hour = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tpGameTime?.hour
        } else {
            tpGameTime?.currentHour
        }

        val minute = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tpGameTime?.minute
        } else {
            tpGameTime?.currentMinute
        }

        if (hour!! < 12) {
            return hour.toString() + ":" + minute + "AM"
        } else {
            return (hour - 12).toString() + ":" + minute + "PM"
        }
    }

    override fun onResume() {
        StringUtils.setTitle(activity as MainActivity)
        etRoomName?.setText("")
        etRoomCaption?.setText("")
        super.onResume()
    }

}// Required empty public constructor
