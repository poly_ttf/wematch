package com.project.wematch.Fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.project.wematch.Activity.MainActivity
import com.project.wematch.Activity.RoomDetailActivity
import com.project.wematch.Adapter.BaseItemListAdapter
import com.project.wematch.Adapter.HomeAdapter
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.DAO.RoomDAO

import com.project.wematch.R
import com.project.wematch.TO.*
import com.project.wematch.Util.*
import kotlinx.android.synthetic.main.fragment_home.*
import rx.Observable
import rx.Subscriber


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment() {

    var v: View? = null

    var rvHome: RecyclerView? = null
    private var subscriber: Subscriber<List<RoomDetailTO>>? = null
    private var joinRoomSubscriber: Subscriber<ActionResultTO>? = null
    private val adapter: HomeAdapter by lazy { HomeAdapter(context) }

    val user: GamerTO by lazy { Gson().fromJson(SharedPrefsUtils.getStringPreference(activity, "user"), GamerTO::class.java) }
    var game: GameTO? = null

    var mode: String = "1"

    companion object {
        fun newInstance(game: GameTO): HomeFragment {
            val args = Bundle()
            args.putSerializable("game", game)

            val fragment: HomeFragment = HomeFragment.newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = container?.inflate(R.layout.fragment_home)

            game = if (arguments?.containsKey("game") == true) {
                arguments?.getSerializable("game") as GameTO
            } else {
                null
            }

            initView()
            getListing()
        }
        return v
    }

    fun initView() {
        rvHome = v?.findViewById<RecyclerView>(R.id.rv_home)
        rvHome?.layoutManager = LinearLayoutManager(context)
        adapter.setCallback(object : BaseItemListAdapter.Callback {
            override fun onLoadMoreResult(offset: Int) {

            }

            override fun onItemSelected(item: Any, holder: RecyclerView.ViewHolder) {
                val intent = Intent(activity, RoomDetailActivity::class.java)
                intent.putExtra("roomId", (item as RoomDetailTO).room_id)
                startActivity(intent)
            }

        })
        adapter.setFollowGamerCallback(object : HomeAdapter.RoomListingCallback {
            override fun onModeClicked(mode: String) {
                game = null
                this@HomeFragment.mode = mode
                listByMode()
            }

            override fun onGamerInfoClick(item: Any, holder: RecyclerView.ViewHolder) {
                (activity as AppCompatActivity).swapFragment(PersonalFragment.newInstance((item as RoomDetailTO).user_id!!), "personal")
            }

            override fun onJoinRoomClick(item: Any, holder: RecyclerView.ViewHolder) {
                joinRoom((item as RoomDetailTO).room_id!!)
            }

        })
        rvHome?.adapter = adapter
    }

    private fun getListing() {
        if (game != null) {
            searchRoomByGameId()
        } else {
            listByMode()
        }
    }

    private fun searchRoomByGameId() {
        val listingTask: Observable<List<RoomDetailTO>>? = RoomDAO(activity).searchRoomByGameID(SharedPrefsUtils.getMyId(activity)!!, game?.id!!)
        subscriber = object : Subscriber<List<RoomDetailTO>>() {
            override fun onError(e: Throwable?) {
            }

            override fun onCompleted() {
            }

            override fun onNext(result: List<RoomDetailTO>?) {
                if (result != null) {
                    adapter.setResult(result, 0)
                    if (result.size == 0) {
                        tv_no_result.visibility = View.VISIBLE
                    } else {
                        tv_no_result.visibility = View.GONE
                    }
                } else {
                    tv_no_result.visibility = View.VISIBLE
                }
            }

        }

        if (listingTask != null) {
            listingTask.subscribe(subscriber)
        }
    }

    private fun listByMode() {
        val listingTask: Observable<List<RoomDetailTO>>? = RoomDAO(activity).listAllRoom(SharedPrefsUtils.getMyId(activity)!!, mode)
        subscriber = object : Subscriber<List<RoomDetailTO>>() {
            override fun onError(e: Throwable?) {
            }

            override fun onCompleted() {
            }

            override fun onNext(result: List<RoomDetailTO>?) {
                if (result != null) {
                    adapter.setResult(result, 0)
                    if (result.size == 0) {
                        tv_no_result.visibility = View.VISIBLE
                    } else {
                        tv_no_result.visibility = View.GONE
                    }
                } else {
                    tv_no_result.visibility = View.VISIBLE
                }
            }

        }

        if (listingTask != null) {
            listingTask.subscribe(subscriber)
        }
    }

    private fun joinRoom(roomId: String) {
        val strUser: String? = SharedPrefsUtils.getStringPreference(activity, "user")
        var user: GamerTO? = null
        if (strUser != null) {
            user = Gson().fromJson(strUser, GamerTO::class.java)
        }

        val joinRoomTask: Observable<ActionResultTO>? = RoomDAO(activity).joinRoom(JoinRoomTO(user?.id, roomId))
        joinRoomSubscriber = object : Subscriber<ActionResultTO>() {
            override fun onNext(result: ActionResultTO?) {
                if (result != null) {
                    if (result.isSuccess) {
                        val messageDialog = AlertDialog.Builder(activity)
                        messageDialog.setTitle(getString(R.string.congratulation))
                        messageDialog.setMessage(getString(R.string.message_success_join_room))
                        messageDialog.setPositiveButton(getString(R.string.confirm), { dialog, which -> Unit })
                        messageDialog.setCancelable(false)
                        messageDialog.show()
                    } else {
                        val messageDialog = AlertDialog.Builder(activity)
                        messageDialog.setTitle(getString(R.string.error))
                        messageDialog.setCancelable(false)
                        messageDialog.setMessage(getString(R.string.message_fail_join_room))
                        messageDialog.setPositiveButton(getString(R.string.confirm), { dialog, which -> Unit })
                        messageDialog.show()
                    }
                    getListing()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {

            }
        }

        joinRoomTask?.subscribe(joinRoomSubscriber)
    }

    override fun onResume() {
        StringUtils.setTitle(activity as MainActivity)
        getListing()
        super.onResume()
    }

}
