package com.project.wematch.Fragment


import android.content.Intent
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.joanzapata.iconify.widget.IconTextView
import com.project.wematch.Activity.*
import com.project.wematch.Adapter.BaseItemListAdapter
import com.project.wematch.Adapter.GameHistoryAdapter
import com.project.wematch.Adapter.SelectGameAdapter
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.DAO.RoomDAO
import com.project.wematch.Listener.FollowGamerListener

import com.project.wematch.R
import com.project.wematch.TO.*
import com.project.wematch.Util.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_personal.*
import rx.Observable
import rx.Subscriber


/**
 * A simple [Fragment] subclass.
 */
class PersonalFragment : BaseFragment() {

    var v: View? = null

    val ivUserPic: ImageView? by lazy { v?.findViewById<ImageView>(R.id.iv_user_pic) }
    val tvUsername: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_username) }
    private val tvDescription: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_description) }

    val tvFollowers: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_follower) }
    val llFollowers: LinearLayout? by lazy { v?.findViewById<LinearLayout>(R.id.ll_follower) }

    val tvFollowing: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_following) }
    val llUserFollowed: LinearLayout? by lazy { v?.findViewById<LinearLayout>(R.id.ll_user_followed) }

    val tvJoints: TextView? by lazy { v?.findViewById<TextView>(R.id.tv_joints) }

    val rvHistory: RecyclerView? by lazy { v?.findViewById<RecyclerView>(R.id.rv_history) }
    val ivMyRoom: IconTextView? by lazy { v?.findViewById<IconTextView>(R.id.iv_my_room) }
    val ivJointRoom: IconTextView? by lazy { v?.findViewById<IconTextView>(R.id.iv_joint_room) }

    val adapter: GameHistoryAdapter? by lazy { GameHistoryAdapter(activity) }

    var followGamerListener: FollowGamerListener? = null

    private var gamer: GamerTO = GamerTO()
    private var historyRoom: List<RoomHistoryTO>? = null

    private var gamerSubscriber: Subscriber<GamerTO>? = null
    private var followGamerSubscriber: Subscriber<ActionResultTO>? = null
    private var roomSubscriber: Subscriber<List<RoomHistoryTO>>? = null

    private var isSelf: Boolean = false

    companion object {
        fun newInstance(gamerId: String): PersonalFragment {
            val args = Bundle()
            args.putString("gamerId", gamerId)

            val fragment: PersonalFragment = newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): PersonalFragment {
            return PersonalFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        gamer.id = arguments?.getString("gamerId")

        val myself: GamerTO = Gson().fromJson(SharedPrefsUtils.getStringPreference(activity, "user"), GamerTO::class.java)
        isSelf = myself.id.equals(gamer.id)
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //getActivity() is fully created in onActivityCreated and instanceOf differentiate it between different Activities
        if (activity is FollowGamerListener)
            followGamerListener = activity as FollowGamerListener
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = container?.inflate(R.layout.fragment_personal)

            initView()
            getGamerDetail()
            getJointRoom()
        }
        return v

    }

    fun initView() {
        rvHistory?.layoutManager = StaggeredGridLayoutManager(3, 1)
        rvHistory?.adapter = adapter
        adapter?.setCallback(object : BaseItemListAdapter.Callback {
            override fun onLoadMoreResult(offset: Int) {

            }

            override fun onItemSelected(item: Any, holder: RecyclerView.ViewHolder) {
                val intent = Intent(activity, RoomDetailActivity::class.java)
                intent.putExtra("roomId", (item as RoomHistoryTO).room_id)
                startActivity(intent)
            }

        })

        llFollowers?.setOnClickListener {
            val intent = Intent(activity, GamerListActivity::class.java)
            intent.putExtra("gamerId", gamer.id)
            intent.putExtra("gamerName", gamer.username)
            intent.putExtra("mode", "0")
            startActivity(intent)
        }

        llUserFollowed?.setOnClickListener {
            val intent = Intent(activity, GamerListActivity::class.java)
            intent.putExtra("gamerId", gamer.id)
            intent.putExtra("gamerName", gamer.username)
            intent.putExtra("mode", "1")
            startActivity(intent)
        }

    }

    fun getGamerDetail() {
        var gamerDetailTask: Observable<GamerTO>? = GamerDAO(activity).searchGamer(gamer.id ?: "0")
        gamerSubscriber = object : Subscriber<GamerTO>() {
            override fun onNext(gamer: GamerTO?) {
                if (gamer != null) {
                    this@PersonalFragment.gamer = gamer
                    getStatistic()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {

            }
        }
        gamerDetailTask?.subscribe(gamerSubscriber)
    }

    fun getStatistic() {
        var gamerDetailTask: Observable<GamerTO>? = GamerDAO(activity).showPersonal(gamer.id ?: "0")
        gamerSubscriber = object : Subscriber<GamerTO>() {
            override fun onNext(gamer: GamerTO?) {
                if (gamer != null) {
                    this@PersonalFragment.gamer.followed = gamer.followed
                    this@PersonalFragment.gamer.following = gamer.following
                    this@PersonalFragment.gamer.join_rooms = gamer.join_rooms
                    updateUI()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {

            }
        }
        gamerDetailTask?.subscribe(gamerSubscriber)
    }

    private fun getJointRoom() {
        var roomTask: Observable<List<RoomHistoryTO>>? = RoomDAO(activity).getJointRoom(gamer.id ?: "0")
        roomSubscriber = object : Subscriber<List<RoomHistoryTO>>() {
            override fun onNext(room: List<RoomHistoryTO>?) {
                if (room != null) {
                    this@PersonalFragment.historyRoom = room
                    adapter?.setResult(this@PersonalFragment.historyRoom!!, this@PersonalFragment.historyRoom!!.size)
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }
        }
        roomTask?.subscribe(roomSubscriber)

    }

    fun updateUI() {
        tvUsername?.text = gamer.username
        tvDescription?.text = gamer.desc
        Picasso.with(context).load(gamer.profile_pic).placeholder(R.drawable.ic_launcher).into(ivUserPic)

        tv_follower?.text = gamer.followed
        tv_following?.text = gamer.following
        tv_joints?.text = gamer.join_rooms
    }

    fun followGamer(gamerId: String) {
        val followGamerTask: Observable<ActionResultTO>? = GamerDAO(activity).followUser(FollowGamerTO(gamerId, SharedPrefsUtils.getMyId(activity)))
        followGamerSubscriber = object : Subscriber<ActionResultTO>() {
            override fun onNext(result: ActionResultTO?) {
                if (result != null) {
                    if (result.isSuccess) {
                        activity.toast(getString(R.string.message_success_follow_gamer))
                        getStatistic()
                    }
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {

            }
        }

        followGamerTask?.subscribe(followGamerSubscriber)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // TODO Add your menu entries here
        if (!isSelf) {
            inflater.inflate(R.menu.menu_follow_gamer, menu)
        } else {
            inflater.inflate(R.menu.menu_more, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_follow -> followGamer(gamer.id!!)
            R.id.action_more -> startActivity(Intent(activity, SettingActivity::class.java))

        }
        return true

    }

    override fun onResume() {
        StringUtils.setTitle(activity as MainActivity)
        getGamerDetail()
        getJointRoom()
        super.onResume()
    }

    override fun onStop() {
        gamerSubscriber?.unsubscribe()
        followGamerSubscriber?.unsubscribe()
        roomSubscriber?.unsubscribe()
        super.onStop()
    }

}
