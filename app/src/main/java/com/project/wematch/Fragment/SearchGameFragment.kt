package com.project.wematch.Fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import com.project.wematch.Activity.MainActivity
import com.project.wematch.DAO.GameDAO

import com.project.wematch.R
import com.project.wematch.TO.*
import com.project.wematch.Util.StringUtils
import com.project.wematch.Util.getSerializable
import com.project.wematch.Util.inflate
import com.project.wematch.Util.swapFragment
import fr.ganfra.materialspinner.MaterialSpinner
import rx.Observable
import rx.Subscriber


/**
 * A simple [Fragment] subclass.
 */
class SearchGameFragment : Fragment() {

    var v: View? = null

    val spiGamePlatform: MaterialSpinner? by lazy { v?.findViewById<MaterialSpinner>(R.id.spi_game_platform) }
    val spiGameType: MaterialSpinner? by lazy { v?.findViewById<MaterialSpinner>(R.id.spi_game_type) }
    val btnSearchGame: Button? by lazy { v?.findViewById<Button>(R.id.btn_search_game) }

    private var gamePlatformList: List<GamePlatformTO>? = null
    private var gameTypeList: List<GameTypeTO>? = null

    var platformItems: Array<String>? = null
    var typeItems: Array<String>? = null

    private var gameTypeSubscriber: Subscriber<List<GameTypeTO>>? = null
    private var gamePlatformSubscriber: Subscriber<List<GamePlatformTO>>? = null

    var createRoom: CreateRoomTO? = null
    val game: GameTO = GameTO()

    companion object {
        fun newInstance(createRoom: CreateRoomTO): SearchGameFragment {
            val args = Bundle()
            args.putSerializable("createRoom", createRoom)

            val fragment: SearchGameFragment = newInstance()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): SearchGameFragment {
            return SearchGameFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = container?.inflate(R.layout.fragment_search_game)

            createRoom = if (arguments?.containsKey("createRoom") == true) {
                arguments?.getSerializable("createRoom") as CreateRoomTO
            } else {
                null
            }

            initView()
            getGamePlatform()
            getGameType()
        }
        return v
    }

    private fun initView() {
        btnSearchGame?.setOnClickListener { searchGame() }
    }

    private fun getGamePlatform() {
        val gamePlatformTask: Observable<List<GamePlatformTO>>? = GameDAO(activity).getGamePlatform()
        gamePlatformSubscriber = object : Subscriber<List<GamePlatformTO>>() {
            override fun onNext(t: List<GamePlatformTO>?) {

                gamePlatformList = t
                platformItems = t?.map { it.name!! }!!.toTypedArray()

                val adapter: ArrayAdapter<String> = ArrayAdapter(activity, android.R.layout.simple_spinner_item, platformItems)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spiGamePlatform?.adapter = adapter
                spiGamePlatform?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        if (p2 >= 0) {
                            createRoom?.gamePlatform = gamePlatformList?.get(p2)
                            game.game_platform_id = gamePlatformList?.get(p2)?.id
                        } else {
                            createRoom?.gamePlatform = null
                            game.game_platform_id = "0"
                        }

                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }

        }

        gamePlatformTask?.subscribe(gamePlatformSubscriber)
    }

    private fun getGameType() {
        val gameTypeTask: Observable<List<GameTypeTO>>? = GameDAO(activity).getGameType()
        gameTypeSubscriber = object : Subscriber<List<GameTypeTO>>() {
            override fun onNext(t: List<GameTypeTO>?) {
                gameTypeList = t
                typeItems = t?.map { it.name!! }!!.toTypedArray()

                val adapter: ArrayAdapter<String> = ArrayAdapter(activity, android.R.layout.simple_spinner_item, typeItems)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spiGameType?.adapter = adapter
                spiGameType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        if (p2 >= 0) {
                            createRoom?.gameType = gameTypeList?.get(p2)
                            game.game_type_id = gameTypeList?.get(p2)?.id
                        } else {
                            createRoom?.gameType = null
                            game.game_type_id = "0"
                        }

                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }

        }

        gameTypeTask?.subscribe(gameTypeSubscriber)
    }

    private fun searchGame() {
        if (isGamePlatformSelected() || isGameTypeSelected()) {
            if (createRoom != null) {
                (activity as AppCompatActivity).swapFragment(SelectGameFragment.newInstance(createRoom!!), "")
            } else {
                (activity as AppCompatActivity).swapFragment(SelectGameFragment.newInstance(game), "")
            }
        }
    }

    private fun isGameTypeSelected(): Boolean = spiGameType?.selectedItemPosition ?: 0 > 0

    private fun isGamePlatformSelected(): Boolean = spiGamePlatform?.selectedItemPosition ?: 0 > 0

    override fun onResume() {
        StringUtils.setTitle(activity as MainActivity)
        super.onResume()
    }

}// Required empty public constructor