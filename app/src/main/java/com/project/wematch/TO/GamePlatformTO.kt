package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by fish on 21/11/2017.
 */
open class GamePlatformTO : Serializable {
    var id: String? = null
    var name: String? = null
    var image: String? = null

    constructor() {

    }

    constructor(id: String?, name: String?, image: String?) {
        this.id = id
        this.name = name
        this.image = image
    }
}