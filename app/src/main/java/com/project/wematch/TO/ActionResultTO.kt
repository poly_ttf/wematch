package com.project.wematch.TO

/**
 * Created by Fish on 2017/11/23.
 */

class ActionResultTO : BaseTO {
    private var result: Int? = null
    var msg: String? = ""

    val isSuccess: Boolean
        get() = result == 1

    constructor(result: Int?, msg: String?) {
        this.result = result
        this.msg = msg
    }

}