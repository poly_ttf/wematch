package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by fish on 21/11/2017.
 */
open class GameTO : Serializable {
    var id: String? = null
    var name: String? = null
    var description: String? = null
    var image: String? = null
    var master_image: String? = null
    var game_type_id: String? = "0"
        set(value) {
            if (value == null) {
                game_type_id = "0"
            } else {
                field = value
            }
        }
    var game_platform_id: String? = "0"
        set(value) {
            if (value == null) {
                game_platform_id = "0"
            } else {
                field = value
            }
        }

    constructor() {

    }

    constructor(id: String?, name: String?, description: String?, image: String?, master_image: String?, game_type_id: String?, game_platform_id: String?) {
        this.id = id
        this.name = name
        this.description = description
        this.image = image
        this.master_image = master_image
        this.game_type_id = game_type_id
        this.game_platform_id = game_platform_id
    }
}