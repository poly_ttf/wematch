package com.project.wematch.TO

/**
 * Created by Fish on 2017/11/22.
 */

class MetaTO {
    val viewType: Int
    val typeIndex: Int

    constructor(viewType: Int) {
        this.viewType = viewType
        this.typeIndex = -1
    }

    constructor(viewType: Int, typeIndex: Int) {
        this.viewType = viewType
        this.typeIndex = typeIndex
    }
}
