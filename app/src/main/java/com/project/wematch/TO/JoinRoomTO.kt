package com.project.wematch.TO

import android.util.Log
import java.io.Serializable

/**
 * Created by Fish on 2017/11/23.
 */

open class JoinRoomTO : Serializable {

    var user_id: String? = null
    var room_id: String? = null

    constructor() {

    }

    constructor(user_id: String?, room_id: String?) {
        this.user_id = user_id
        this.room_id = room_id
    }

}