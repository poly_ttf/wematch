package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by fish on 21/11/2017.
 */
open class GameTypeTO : Serializable {
    var id: String? = null
    var name: String? = null

    constructor() {

    }

    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }
}