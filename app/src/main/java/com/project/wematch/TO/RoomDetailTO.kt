package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by fish on 26/11/2017.
 */

open class RoomDetailTO : Serializable {

    var room_id: String? = null
    var room_name: String? = null
    var id: String? = null
    var user_id: String? = null
    var name: String? = null
    var game_id: String? = null
    var caption: String? = null
    var mode: String? = null
    var game_date: String? = null
    var game_time: String? = null
    var is_close: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var description: String? = null
    var image: String? = null
    var master_image: String? = null
    var game_type_id: String? = null
    var game_platform_id: String? = null
    var username: String? = null
    var email: String? = null
    var device_id: String? = null
    var profile_pic: String? = null
    var password: String? = null
    var desc: String? = null
    var total_join: String? = null
    var has_notification: Int = 0
    var is_join: Boolean = false

    constructor() {}

}