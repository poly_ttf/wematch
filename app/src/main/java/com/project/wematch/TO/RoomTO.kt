package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by Fish on 2017/11/23.
 */

open class RoomTO : Serializable {

    var id: String? = null
    var user_id: String? = null
    var name: String? = null
    var game_id: String? = null
    var caption: String? = null
    var mode: String? = null
    var game_date: String? = null
    var game_time: String? = null
    var is_close: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var description: String? = null
    var image: String? = null
    var master_image: String? = null
    var game_type_id: String? = null
    var game_platform_id: String? = null
    var total_join: String? = null
    var is_join: Boolean = false

    constructor() {

    }

    constructor(id: String?, user_id: String?, name: String?, game_id: String?, caption: String?, mode: String?, game_date: String?, game_time: String?, is_close: String?, created_at: String?, updated_at: String?, description: String?, image: String?, master_image: String?, game_type_id: String?, game_platform_id: String?, total_join: String?, is_join: Boolean) {
        this.id = id
        this.user_id = user_id
        this.name = name
        this.game_id = game_id
        this.caption = caption
        this.mode = mode
        this.game_date = game_date
        this.game_time = game_time
        this.is_close = is_close
        this.created_at = created_at
        this.updated_at = updated_at
        this.description = description
        this.image = image
        this.master_image = master_image
        this.game_type_id = game_type_id
        this.game_platform_id = game_platform_id
        this.total_join = total_join
        this.is_join = is_join
    }

}