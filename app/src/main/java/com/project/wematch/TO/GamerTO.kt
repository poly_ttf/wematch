package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by fish on 21/11/2017.
 */
open class GamerTO : Serializable {
    var id: String? = null
    var username: String? = null
    var email: String? = null
    var desc: String? = null
    var deviceId: String? = null
    var password: String? = null
    var profile_pic: String? = null
    var join_rooms: String? = null
    var followed: String? = null
    var following: String? = null

    constructor() {

    }

    constructor(id: String?, username: String?, email: String?, desc: String?, profile_pic: String?) {
        this.id = id
        this.username = username
        this.email = email
        this.desc = desc
        this.profile_pic = profile_pic
    }
}
