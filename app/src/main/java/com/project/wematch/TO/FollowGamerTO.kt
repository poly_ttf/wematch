package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by frankiefong on 23/11/2017.
 */

open class FollowGamerTO : Serializable {
    var user_id: String? = null
    var follower_id: String? = null

    constructor() {

    }

    constructor(user_id: String?, follower_id: String?) {
        this.user_id = user_id
        this.follower_id = follower_id
    }
}