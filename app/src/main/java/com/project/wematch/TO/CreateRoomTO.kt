package com.project.wematch.TO

import java.io.Serializable

/**
 * Created by frankiefong on 23/11/2017.
 */

open class CreateRoomTO : Serializable {
    var room: RoomTO? = null
    var game: GameTO? = null
    var gamePlatform: GamePlatformTO? = null
    var gameType: GameTypeTO? = null

    constructor() {

    }
}