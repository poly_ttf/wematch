package com.project.wematch.Widget

import android.content.Context
import android.content.Intent
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.project.wematch.R
import com.project.wematch.Util.swapFragment

/**
 * Created by fish on 20/11/2017.
 */

class TabComponent(val context: AppCompatActivity,
                   val tabLayout: TabLayout,
                   val tabModels: List<TabVM>,
                   val renderStrategy: TabRenderStrategy,
                   val defaultTabModelId: Int,
                   val onTabSelected: (tabModel: TabVM, tabLayout: TabLayout) -> Unit,
                   val onTabUnselected: (tabModel: TabVM, tabLayout: TabLayout) -> Unit) {

    private var currentTabId: Int? = null

    private fun getDefaultTab(): TabLayout.Tab? {
        return (0..(tabLayout.tabCount - 1))
                .map { tabLayout.getTabAt(it) }
                .firstOrNull { (it?.customView?.tag as TabVM?)?.tabId == defaultTabModelId }
    }

    private fun getCurrentTab(): TabLayout.Tab? {
        return (0..(tabLayout.tabCount - 1))
                .map { tabLayout.getTabAt(it) }
                .firstOrNull { (it?.customView?.tag as TabVM?)?.tabId == currentTabId }
    }

    private fun selectTab(tab: TabLayout.Tab?) {
        val customTag = tab?.customView
        val model = customTag?.tag as TabVM

        currentTabId = model.tabId
        renderStrategy.selectTab(model, customTag, context)
        onTabSelected(model, tabLayout)
        context.swapFragment(model.fragment)
    }

    private fun unselectTab(tab: TabLayout.Tab?) {
        val customTag = tab?.customView
        val model = customTag?.tag as TabVM
        if (model.fragment != null) {
            renderStrategy.unselectTab(model, customTag, context)
            onTabUnselected(model, tabLayout)
        }
    }

    fun initTabLayout() {
        tabModels
                .map { tabVM -> renderStrategy.render(tabVM, tabLayout, context) }
                .forEach { tab ->
                    tabLayout.addTab(tab)
                }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val customTag = tab.customView
                val model = customTag?.tag as TabVM
                if (model.fragment != null) {
                    selectTab(tab)
                    (0..tabLayout.tabCount - 1)
                            .filter { tab != tabLayout.getTabAt(it) }
                            .forEach { unselectTab(tabLayout.getTabAt(it)) }
                } else {
                    val intent = Intent(context, model.activity)
                    context.startActivity(intent)
                    getCurrentTab()?.select()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })

        selectTab(getDefaultTab())
    }

}

interface TabRenderStrategy {
    fun render(viewModel: TabVM, tabLayout: TabLayout, context: Context): TabLayout.Tab
    fun selectTab(viewModel: TabVM, tabView: View?, context: Context)
    fun unselectTab(viewModel: TabVM, tabView: View?, context: Context)
}

class TextWithIconRenderStrategy : TabRenderStrategy {
    override fun unselectTab(viewModel: TabVM, tabView: View?, context: Context) {
        val view = tabView?.findViewById<TextView>(R.id.tv_tab)
        view?.setTextColor(ContextCompat.getColor(context, viewModel.defaultColorId))
        view?.setCompoundDrawablesWithIntrinsicBounds(0, viewModel.defaultImg, 0, 0)
    }

    override fun selectTab(viewModel: TabVM, tabView: View?, context: Context) {
        val view = tabView?.findViewById<TextView>(R.id.tv_tab)
        view?.setTextColor(ContextCompat.getColor(context, viewModel.selectedColorId))
        view?.setCompoundDrawablesWithIntrinsicBounds(0, viewModel.selectedImg, 0, 0)
    }

    override fun render(viewModel: TabVM, tabLayout: TabLayout, context: Context): TabLayout.Tab {
        val customTab = (LayoutInflater.from(context).inflate(R.layout.tab_main, tabLayout, false) as View).findViewById<TextView>(R.id.tv_tab)
        customTab.text = viewModel.title
        customTab.setTextColor(ContextCompat.getColor(context, viewModel.defaultColorId))
        customTab.setCompoundDrawablesWithIntrinsicBounds(0, viewModel.defaultImg, 0, 0)
        customTab.tag = viewModel

        return tabLayout.newTab().setCustomView(customTab)
    }
}

class IconRenderStrategy : TabRenderStrategy {
    override fun unselectTab(viewModel: TabVM, tabView: View?, context: Context) {
        val view = tabView?.findViewById<ImageView>(R.id.iv_tab)
        view?.setImageResource(viewModel.defaultImg)

    }

    override fun selectTab(viewModel: TabVM, tabView: View?, context: Context) {
        val view = tabView?.findViewById<ImageView>(R.id.iv_tab)
        view?.setImageResource(viewModel.selectedImg)
    }

    override fun render(viewModel: TabVM, tabLayout: TabLayout, context: Context): TabLayout.Tab {
        val customTab = (LayoutInflater.from(context).inflate(R.layout.tab_main, tabLayout, false) as View).findViewById<ImageView>(R.id.iv_tab)
        customTab.setImageResource(viewModel.defaultImg)
        customTab.tag = viewModel

        return tabLayout.newTab().setCustomView(customTab)
    }
}

data class TabVM(val tabId: Int, val title: String, val defaultImg: Int, val selectedImg: Int, val defaultColorId: Int, val selectedColorId: Int, var fragment: Fragment?, var activity: Class<FragmentActivity>?)
