package com.project.wematch.DAO

import android.content.Context
import com.project.wematch.DAO.API.APIBuilder
import com.project.wematch.DAO.APITO.BaseAPITO
import com.project.wematch.R
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.reflect.Type


/**
 * Created by fish on 21/11/2017.
 */

abstract class BaseDAO {

    val context: Context
    val basedHost: String

    constructor(context: Context) {
        this.context = context
        this.basedHost = context.getString(R.string.based_api_host)
    }

    fun getDefaultAPI(apiTOType: Type): Retrofit {
        return APIBuilder.getAPIClient(basedHost, getFactory(apiTOType))
    }

    private fun getFactory(apiTOType: Type): MoshiConverterFactory {
        val moshi = Moshi.Builder().build()
        moshi.adapter<Any>(apiTOType)
        return MoshiConverterFactory.create(moshi).asLenient()
    }

    internal inner class DefaultTransformer<T : BaseAPITO<*>, V> : Observable.Transformer<T, V> {

        override fun call(observable: Observable<T>): Observable<V> {
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map { apiResult -> apiResult.getValue() as V }
        }
    }

}