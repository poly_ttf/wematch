package com.project.wematch.DAO.APITO

import com.project.wematch.TO.GamerTO

/**
 * Created by fish on 22/11/2017.
 */

class APIGamerTO : BaseAPITO<GamerTO>() {

    private var id: String? = null
    private var username: String? = null
    private var email: String? = null
    private var desc: String? = null
    private var profile_pic: String? = null

    override fun getValue(): GamerTO {
        return GamerTO(id, username, email, desc, profile_pic)
    }
}