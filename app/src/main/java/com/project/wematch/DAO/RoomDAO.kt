package com.project.wematch.DAO

import android.content.Context
import com.project.wematch.DAO.API.GamerAPI
import com.project.wematch.DAO.API.RoomAPI
import com.project.wematch.DAO.APITO.APIActionResultTO
import com.project.wematch.DAO.APITO.APIGamerTO
import com.project.wematch.DAO.APITO.APIJoinRoomTO
import com.project.wematch.DAO.APITO.APIRoomTO
import com.project.wematch.DAO.Adapters.APICreateGamerAdapter
import com.project.wematch.DAO.Adapters.APIHomeListingAdapter
import com.project.wematch.R
import com.project.wematch.TO.*
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable
import rx.schedulers.Schedulers
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by fish on 21/11/2017.
 */

class RoomDAO(context: Context) : BaseDAO(context) {

    fun createRoom(body: RoomTO): Observable<RoomTO> {
        return getDefaultAPI(APIRoomTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .createRoom(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun joinRoom(body: JoinRoomTO): Observable<ActionResultTO> {
        return getDefaultAPI(APIActionResultTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .joinRoom(body)
                .compose(DefaultTransformer<APIJoinRoomTO, ActionResultTO>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun listAllRoom(gamerId: String, mode: String): Observable<List<RoomDetailTO>> {
        return getDefaultAPI(APIRoomTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .listAllRoom(gamerId, mode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchRoomByGameID(userId: String, gameId: String): Observable<List<RoomDetailTO>> {
        return getDefaultAPI(APIRoomTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .searchRoomByGameID(userId, gameId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getJointRoom(gamerId: String): Observable<List<RoomHistoryTO>> {
        return getDefaultAPI(APIRoomTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .getJointRoom(gamerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getRoomDetail(roomId: String, userId: String): Observable<List<RoomDetailTO>> {
        return getDefaultAPI(APIRoomTO::class.java)
                .create<RoomAPI>(RoomAPI::class.java)
                .getRoomDetail(roomId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}