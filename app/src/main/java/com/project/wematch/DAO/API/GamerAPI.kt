package com.project.wematch.DAO.API

import com.project.wematch.DAO.APITO.APIFollowUserTO
import com.project.wematch.DAO.APITO.APIGamerTO
import com.project.wematch.TO.FollowGamerTO
import com.project.wematch.TO.GamerTO
import retrofit2.http.*
import rx.Observable

/**
 * Created by fish on 21/11/2017.
 */
interface GamerAPI {

    @Headers("Content-Type: application/json")
    @POST("createGamer")
    fun createGamer(@Body body: GamerTO): Observable<APIGamerTO>

    @Headers("Content-Type: application/json")
    @POST("authGamer")
    fun authGamer(@Body body: GamerTO): Observable<APIGamerTO>

    @Headers("Content-Type: application/json")
    @POST("updateGamer/{gamerId}")
    fun updateGamer(@Path("gamerId") id: String, @Body body: GamerTO): Observable<GamerTO>

    @Headers("Content-Type: application/json")
    @GET("searchGamer/{gamerId}")
    fun searchGamer(@Path("gamerId") id: String): Observable<APIGamerTO>

    @Headers("Content-Type: application/json")
    @GET("showPersonal/{gamerId}")
    fun showPersonal(@Path("gamerId") id: String): Observable<GamerTO>

    @Headers("Content-Type: application/json")
    @POST("followUser")
    fun followGamer(@Body body: FollowGamerTO): Observable<APIFollowUserTO>

    @Headers("Content-Type: application/json")
    @GET("showFollowers/{gamerId}")
    fun showFollowers(@Path("gamerId") id: String): Observable<List<GamerTO>>

    @Headers("Content-Type: application/json")
    @GET("showUserFollowed/{gamerId}")
    fun showUserFollowed(@Path("gamerId") id: String): Observable<List<GamerTO>>

}
