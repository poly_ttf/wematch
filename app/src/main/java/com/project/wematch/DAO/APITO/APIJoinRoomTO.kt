package com.project.wematch.DAO.APITO

import com.project.wematch.TO.ActionResultTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIJoinRoomTO : BaseAPITO<ActionResultTO>() {

    var user_id: String? = null
    var room_id: String? = null
    var updated_at: String? = null
    var created_at: String? = null
    var id: String? = null

    override fun getValue(): ActionResultTO {
        if (user_id != null && room_id != null) {
            return return ActionResultTO(1, null)
        }
        return return ActionResultTO(0, null)
    }
}