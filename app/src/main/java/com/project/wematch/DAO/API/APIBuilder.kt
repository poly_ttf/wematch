package com.project.wematch.DAO.API

import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory


/**
 * Created by fish on 21/11/2017.
 */

object APIBuilder {

    fun getAPIClient(basedHost: String, factory: Converter.Factory): Retrofit {
        val factories = ArrayList<Converter.Factory>(1)
        factories.add(factory)
        return getAPIClient(basedHost, factories)
    }

    fun getAPIClient(basedHost: String, factories: List<Converter.Factory>): Retrofit {
        val builder = Retrofit.Builder().baseUrl(basedHost)
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        for (factory in factories) {
            builder.addConverterFactory(factory)
        }
        return builder.build()
    }
}