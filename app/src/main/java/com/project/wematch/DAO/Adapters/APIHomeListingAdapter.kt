package com.project.wematch.DAO.Adapters

import android.content.Context
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Created by fish on 22/11/2017.
 */
class APIHomeListingAdapter(val context: Context) : Converter.Factory() {
    override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, *>? {
        return Converter<ResponseBody, String> { value -> value.string() }
    }
}