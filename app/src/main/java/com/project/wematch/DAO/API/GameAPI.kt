package com.project.wematch.DAO.API

import com.project.wematch.TO.ActionResultTO
import com.project.wematch.TO.GamePlatformTO
import com.project.wematch.TO.GameTO
import com.project.wematch.TO.GameTypeTO
import retrofit2.http.*
import rx.Observable

/**
 * Created by fish on 21/11/2017.
 */
interface GameAPI {

    @Headers("Content-Type: application/json")
    @POST("addFavouriteGame")
    fun addFavouriteGame(@Body body: HashMap<String, String>): Observable<ActionResultTO>

    @Headers("Content-Type: application/json")
    @GET("searchGameByPlatform/{id}")
    fun searchGameByPlatform(@Path("id") platformId: String): Observable<String>

    @Headers("Content-Type: application/json")
    @GET("searchGameByType/{id}")
    fun searchGameByType(@Path("id") gameTypeId: String): Observable<String>

    @Headers("Content-Type: application/json")
    @GET("searchGameByTypePlatform/{typeId}/{platformId}")
    fun searchGameByTypePlatform(@Path("typeId") typeId: String, @Path("platformId") platformId: String): Observable<List<GameTO>>

    @Headers("Content-Type: application/json")
    @GET("gameType")
    fun getGameType(): Observable<List<GameTypeTO>>

    @Headers("Content-Type: application/json")
    @GET("gamePlatform")
    fun getGamePlatform(): Observable<List<GamePlatformTO>>

    @Headers("Content-Type: application/json")
    @GET("games")
    fun getGames(): Observable<List<GameTO>>

    @Headers("Content-Type: application/json")
    @GET("searchGamer/{id}")
    fun getGameDetail(@Path("id") gameId: String): Observable<String>
}