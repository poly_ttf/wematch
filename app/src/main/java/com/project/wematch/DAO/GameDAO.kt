package com.project.wematch.DAO

import android.content.Context
import com.project.wematch.DAO.API.GameAPI
import com.project.wematch.DAO.APITO.APIActionResultTO
import com.project.wematch.DAO.APITO.APIGameTO
import com.project.wematch.TO.ActionResultTO
import com.project.wematch.TO.GamePlatformTO
import com.project.wematch.TO.GameTO
import com.project.wematch.TO.GameTypeTO
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by fish on 21/11/2017.
 */

class GameDAO(context: Context) : BaseDAO(context) {

    fun getGameType(): Observable<List<GameTypeTO>> {
        return getDefaultAPI(GameTypeTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .getGameType()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getGamePlatform(): Observable<List<GamePlatformTO>> {
        return getDefaultAPI(GamePlatformTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .getGamePlatform()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getGames(): Observable<List<GameTO>> {
        return getDefaultAPI(APIGameTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .getGames()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchGameByType(typeId: String): Observable<String> {
        return getDefaultAPI(APIGameTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .searchGameByType(typeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchGameByPlatform(platformId: String): Observable<String> {
        return getDefaultAPI(APIGameTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .searchGameByPlatform(platformId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchGameByTypePlatform(typeId: String, platformId: String): Observable<List<GameTO>> {
        return getDefaultAPI(APIGameTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .searchGameByTypePlatform(typeId, platformId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun addFavouriteGame(body: HashMap<String, String>): Observable<ActionResultTO> {
        return getDefaultAPI(APIActionResultTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .addFavouriteGame(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getGameDetail(gameId: String): Observable<String> {
        return getDefaultAPI(APIActionResultTO::class.java)
                .create<GameAPI>(GameAPI::class.java)
                .getGameDetail(gameId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}