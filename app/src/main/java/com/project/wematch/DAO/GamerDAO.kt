package com.project.wematch.DAO

import android.content.Context
import com.project.wematch.DAO.API.GamerAPI
import com.project.wematch.DAO.APITO.APIFollowUserTO
import com.project.wematch.DAO.APITO.APIGamerTO
import com.project.wematch.TO.ActionResultTO
import com.project.wematch.TO.FollowGamerTO
import com.project.wematch.TO.GamerTO
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by fish on 21/11/2017.
 */

class GamerDAO(context: Context) : BaseDAO(context) {

    fun createGamer(body: GamerTO): Observable<GamerTO> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .createGamer(body)
                .compose(DefaultTransformer<APIGamerTO, GamerTO>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun authGamer(body: GamerTO): Observable<GamerTO> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .authGamer(body)
                .compose(DefaultTransformer<APIGamerTO, GamerTO>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun updateGamer(gamerId: String, body: GamerTO): Observable<GamerTO> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .updateGamer(gamerId, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchGamer(gamerId: String): Observable<GamerTO> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .searchGamer(gamerId)
                .compose(DefaultTransformer<APIGamerTO, GamerTO>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun showPersonal(gamerId: String): Observable<GamerTO> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .showPersonal(gamerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun followUser(body: FollowGamerTO): Observable<ActionResultTO> {
        return getDefaultAPI(APIFollowUserTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .followGamer(body)
                .compose(DefaultTransformer<APIFollowUserTO, ActionResultTO>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun showFollowers(gamerId: String): Observable<List<GamerTO>> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .showFollowers(gamerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun showUserFollowed(gamerId: String): Observable<List<GamerTO>> {
        return getDefaultAPI(APIGamerTO::class.java)
                .create<GamerAPI>(GamerAPI::class.java)
                .showUserFollowed(gamerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}