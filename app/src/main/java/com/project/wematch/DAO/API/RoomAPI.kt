package com.project.wematch.DAO.API

import com.project.wematch.DAO.APITO.APIActionResultTO
import com.project.wematch.DAO.APITO.APIJoinRoomTO
import com.project.wematch.TO.JoinRoomTO
import com.project.wematch.TO.RoomDetailTO
import com.project.wematch.TO.RoomHistoryTO
import com.project.wematch.TO.RoomTO
import retrofit2.http.*
import rx.Observable

/**
 * Created by fish on 21/11/2017.
 */
interface RoomAPI {
    @Headers("Content-Type: application/json")
    @GET("listAllRoom/{gamerId}/{mode}")
    fun listAllRoom(@Path("gamerId") gamerId: String, @Path("mode") mode: String): Observable<List<RoomDetailTO>>

    @Headers("Content-Type: application/json")
    @GET("searchRoomByGameID/{userId}/{gameId}")
    fun searchRoomByGameID(@Path("userId") userId: String, @Path("gameId") gameId: String): Observable<List<RoomDetailTO>>

    @Headers("Content-Type: application/json")
    @GET("joinedRoom/{userId}")
    fun getJointRoom(@Path("userId") userId: String): Observable<List<RoomHistoryTO>>

    @Headers("Content-Type: application/json")
    @GET("room/{roomId}/{userId}")
    fun getRoomDetail(@Path("roomId") roomId: String, @Path("userId") userId: String): Observable<List<RoomDetailTO>>

    @Headers("Content-Type: application/json")
    @POST("createRoom")
    fun createRoom(@Body body: RoomTO): Observable<RoomTO>

    @Headers("Content-Type: application/json")
    @POST("joinRoom")
    fun joinRoom(@Body body: JoinRoomTO): Observable<APIJoinRoomTO>

    @Headers("Content-Type: application/json")
    @POST("leaveRoom")
    fun leaveRoom(@Body body: HashMap<String, String>): Observable<String>
}