package com.project.wematch.DAO.APITO

import com.project.wematch.TO.GameTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIGameTO : BaseAPITO<GameTO>() {

    var id: String? = null
    var name: String? = null
    var description: String? = null
    var image: String? = null
    var master_image: String? = null
    var game_type_id: String? = null
    var game_platform_id: String? = null

    override fun getValue(): GameTO {
        return GameTO(id, name, description, image, master_image, game_type_id, game_platform_id)
    }
}