package com.project.wematch.DAO.APITO

import com.project.wematch.TO.ActionResultTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIActionResultTO : BaseAPITO<ActionResultTO>() {

    private val result: Int? = null

    override fun getValue(): ActionResultTO {
        return ActionResultTO(result, null)
    }
}