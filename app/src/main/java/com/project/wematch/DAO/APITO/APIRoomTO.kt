package com.project.wematch.DAO.APITO

import com.project.wematch.TO.RoomTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIRoomTO : BaseAPITO<RoomTO>() {
    var id: String? = null
    var user_id: String? = null
    var name: String? = null
    var game_id: String? = null
    var caption: String? = null
    var mode: String? = null
    var game_date: String? = null
    var game_time: String? = null
    var is_close: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var description: String? = null
    var image: String? = null
    var master_image: String? = null
    var game_type_id: String? = null
    var game_platform_id: String? = null
    var total_join: String? = null
    var room_id: String? = null
    var has_notification: String? = null
    var is_join: Int = 0

    override fun getValue(): RoomTO {
        return RoomTO(id, user_id, name, game_id, caption, mode, game_date, game_time, is_close, created_at, updated_at, description, image, master_image, game_type_id, game_platform_id, total_join, is_join == 1)
    }
}