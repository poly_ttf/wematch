package com.project.wematch.DAO.APITO

import com.project.wematch.TO.ActionResultTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIFollowUserTO : BaseAPITO<ActionResultTO>() {

    var user_id: String? = null
    var follower_id: String? = null

    override fun getValue(): ActionResultTO {
        if (user_id != null && follower_id != null) {
            return return ActionResultTO(1, null)
        }
        return return ActionResultTO(0, null)
    }
}