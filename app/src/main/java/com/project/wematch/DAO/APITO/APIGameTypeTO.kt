package com.project.wematch.DAO.APITO

import com.project.wematch.TO.GameTypeTO

/**
 * Created by Fish on 2017/11/23.
 */

class APIGameTypeTO : BaseAPITO<GameTypeTO>() {

    var id: String? = null
    var name: String? = null

    override fun getValue(): GameTypeTO {
        return GameTypeTO(id, name)
    }
}