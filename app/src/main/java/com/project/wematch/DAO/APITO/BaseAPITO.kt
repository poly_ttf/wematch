package com.project.wematch.DAO.APITO

/**
 * Created by fish on 22/11/2017.
 */
abstract class BaseAPITO<TO> {
    abstract fun getValue(): TO
}