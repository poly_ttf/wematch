package com.project.wematch.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.google.firebase.iid.FirebaseInstanceId
import com.project.wematch.R
import com.project.wematch.Util.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_splash.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d("firebase", "refreshToken : $refreshedToken")
        initSplashImage()

        Observable.timer(4, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {

            var intent: Intent? = null
            var user: String? = SharedPrefsUtils.getStringPreference(this, "user")
            if (user != null) {
                intent = Intent(this, MainActivity::class.java)
            } else {
                intent = Intent(this, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()
        }

    }

    private fun initSplashImage() {
        val mIvSplash = findViewById<ImageView>(R.id.iv_splash) as ImageView
        mIvSplash.startAnimation(AnimationUtils.loadAnimation(this, R.anim.pop_in))
        Observable.timer(1900, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
            tv_slogan.text = "Match Everyone Now !"
        }
    }

}
