package com.project.wematch.Activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import com.google.gson.Gson
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.R
import com.project.wematch.TO.GamerTO
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.StringUtils
import com.project.wematch.Util.ViewUtils
import com.project.wematch.Util.toast
import kotlinx.android.synthetic.main.activity_register.*
import rx.Observable
import rx.Subscriber

class LoginActivity : AppCompatActivity() {

    val etEmail: EditText by lazy { findViewById<EditText>(R.id.et_email) }
    val etPassword: EditText by lazy { findViewById<EditText>(R.id.et_password) }

    private var subscriber: Subscriber<GamerTO>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun login(v: View) {
        if (isValidEmail() && isValidPassword()) {
            doLogin()
        }
    }

    private fun isValidEmail(): Boolean {
        if (TextUtils.isEmpty(etEmail.text.toString())) {
            etEmail.error = getString(R.string.error_empty_text)
            return false
        } else if (!StringUtils.isValidEmail(etEmail.text.toString())) {
            etEmail.error = getString(R.string.error_invalid_email)
            return false
        }
        return true
    }

    private fun isValidPassword(): Boolean {
        if (TextUtils.isEmpty(etPassword.text.toString())) {
            etPassword.error = getString(R.string.error_empty_text)
            return false
        } else if (!StringUtils.isValidPassword(etPassword.text.toString())) {
            etPassword.error = getString(R.string.error_invalid_password)
            return false
        }
        return true
    }

    private fun doLogin() {
        val gamer = GamerTO()
        gamer.email = etEmail.text.toString()
        gamer.password = etPassword.text.toString()

        val loginTask: Observable<GamerTO>? = GamerDAO(this).authGamer(gamer)
        subscriber = object : Subscriber<GamerTO>() {
            override fun onNext(t: GamerTO?) {
                if (t != null) {
                    SharedPrefsUtils.setStringPreference(this@LoginActivity, "user", Gson().toJson(t))
                }
            }

            override fun onCompleted() {
                toast(getString(R.string.message_success_login))
                var intent: Intent? = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }

            override fun onError(t: Throwable?) {
                val confirmLeaveDialog = android.support.v7.app.AlertDialog.Builder(this@LoginActivity)
                confirmLeaveDialog.setTitle(getString(R.string.error))
                confirmLeaveDialog.setMessage(getString(R.string.error_wrong_auth))
                confirmLeaveDialog.setPositiveButton(getString(R.string.confirm), null)
                confirmLeaveDialog.show()
            }

        }

        loginTask?.subscribe(subscriber)
    }

    fun registerNow(v: View) {
        var intent: Intent? = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP)
            if (!ViewUtils.isPointInsideView(ev.rawX, ev.rawY, et_email) && !ViewUtils.isPointInsideView(ev.rawX, ev.rawY, et_password)) {
                ViewUtils.hideKeyboard(this)
            }
        return super.dispatchTouchEvent(ev)
    }

}
