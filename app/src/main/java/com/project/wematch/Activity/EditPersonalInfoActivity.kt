package com.project.wematch.Activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.R
import com.project.wematch.TO.GamerTO
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_personal_info.*
import rx.Observable
import rx.Subscriber

class EditPersonalInfoActivity : AppCompatActivity() {

    private val etUsername: EditText by lazy { findViewById<EditText>(R.id.et_username) }
    private val etDescription: EditText by lazy { findViewById<EditText>(R.id.et_description) }

    private var subscriber: Subscriber<GamerTO>? = null

    private var gamer: GamerTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_personal_info)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setTitle("Edit Infomation")

        getGamerDetail()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> savePersonalInfo()
            android.R.id.home -> finish()
        }
        return true

    }

    fun savePersonalInfo() {
        if (isValidUsername()) {
            doSave()
        }
    }

    private fun isValidUsername(): Boolean {
        if (TextUtils.isEmpty(etUsername.text.toString())) {
            etUsername.error = getString(R.string.error_empty_text)
            return false
        }
        return true
    }

    fun getGamerDetail() {
        val gamerDetailTask: Observable<GamerTO>? = GamerDAO(this).searchGamer(SharedPrefsUtils.getMyId(this) ?: "0")
        subscriber = object : Subscriber<GamerTO>() {
            override fun onNext(gamer: GamerTO?) {
                if (gamer != null) {
                    this@EditPersonalInfoActivity.gamer = gamer
                    updateUI()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {

            }
        }

        gamerDetailTask?.subscribe(subscriber)
    }

    fun updateUI() {
        etUsername.setText(gamer?.username)
        etDescription.setText(gamer?.desc)
        Picasso.with(this).load(gamer?.profile_pic).placeholder(R.drawable.ic_launcher).into(iv_user_pic)
    }

    private fun doSave() {
        gamer?.username = etUsername.text.toString()
        gamer?.desc = etDescription.text.toString()

        val registerTask: Observable<GamerTO>? = GamerDAO(this).updateGamer(gamer!!.id!!, gamer!!)
        subscriber = object : Subscriber<GamerTO>() {
            override fun onNext(t: GamerTO?) {
            }

            override fun onCompleted() {
                toast("Save info successful!")
                finish()
            }

            override fun onError(t: Throwable?) {
            }

        }

        registerTask?.subscribe(subscriber)
    }

}
