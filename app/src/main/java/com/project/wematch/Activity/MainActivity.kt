package com.project.wematch.Activity

import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.gson.Gson
import com.joanzapata.iconify.Iconify
import com.joanzapata.iconify.fonts.*
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.DAO.RoomDAO
import com.project.wematch.Fragment.CreateRoomFragment
import com.project.wematch.Fragment.HomeFragment
import com.project.wematch.Fragment.PersonalFragment
import com.project.wematch.Fragment.SearchGameFragment
import com.project.wematch.Listener.FollowGamerListener
import com.project.wematch.R
import com.project.wematch.TO.*
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.toast
import com.project.wematch.Widget.IconRenderStrategy
import com.project.wematch.Widget.TabComponent
import com.project.wematch.Widget.TabVM
import kotlinx.android.synthetic.main.activity_main.*
import rx.Observable
import rx.Subscriber


class MainActivity : AppCompatActivity() {

    private var followGamerSubscriber: Subscriber<ActionResultTO>? = null

    private val gamer: GamerTO by lazy { Gson().fromJson(SharedPrefsUtils.getStringPreference(this@MainActivity, "user"), GamerTO::class.java) }

    open val tabModels by lazy {
        listOf(
                TabVM(TAB_HOME, "", R.drawable.ic_home, R.drawable.ic_home_active, R.color.greyLight, R.color.colorPrimary, HomeFragment(), null),
                TabVM(TAB_CREATE_POST, "", R.drawable.ic_create, R.drawable.ic_create_active, R.color.greyLight, R.color.colorPrimary, CreateRoomFragment(), null),
                TabVM(TAB_SEARCH, "", R.drawable.ic_search, R.drawable.ic_search_active, R.color.greyLight, R.color.colorPrimary, SearchGameFragment(), null),
                TabVM(TAB_PERSONAL, "", R.drawable.ic_personal, R.drawable.ic_personal_active, R.color.greyLight, R.color.colorPrimary, PersonalFragment.newInstance(gamer.id!!), null)
        )
    }

    companion object {
        val TAB_HOME = 0
        val TAB_CREATE_POST = 1
        val TAB_SEARCH = 2
        val TAB_PERSONAL = 3
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initIconfy()
        initTabLayout()
        initView()
    }

    fun initIconfy() {
        Iconify.with(FontAwesomeModule())
                .with(EntypoModule())
                .with(TypiconsModule())
                .with(MaterialModule())
                .with(MaterialCommunityModule())
                .with(MeteoconsModule())
                .with(WeathericonsModule())
                .with(SimpleLineIconsModule())
                .with(IoniconsModule())
    }

    fun initTabLayout() {
        TabComponent(this,
                tabLayout = this.tl_main,
                tabModels = tabModels,
                renderStrategy = IconRenderStrategy(),
                defaultTabModelId = TAB_HOME,
                onTabSelected = { tabModel: TabVM, tabLayout: TabLayout -> popAllBackStack() },
                onTabUnselected = { tabModel: TabVM, tabLayout: TabLayout -> Unit }).initTabLayout()

    }

    fun popAllBackStack() {
        val stackCount = supportFragmentManager.backStackEntryCount
        (0 until stackCount).forEach { supportFragmentManager.popBackStack() }

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
    }

    private fun initView() {

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            if (supportFragmentManager.backStackEntryCount == 1) {
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
                supportActionBar?.setHomeButtonEnabled(false)
            }
            supportFragmentManager.popBackStack()
        } else {
            val confirmLeaveDialog = AlertDialog.Builder(this)
            confirmLeaveDialog.setTitle(getString(R.string.leave))
            confirmLeaveDialog.setMessage(getString(R.string.confirm_leave))
            confirmLeaveDialog.setNegativeButton(getString(R.string.cancel), null)
            confirmLeaveDialog.setPositiveButton(getString(R.string.confirm), { dialog, which -> finish() })
            confirmLeaveDialog.show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
