package com.project.wematch.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import com.project.wematch.Adapter.BaseItemListAdapter
import com.project.wematch.Adapter.GamerListAdapter
import com.project.wematch.Adapter.HomeAdapter
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.DAO.RoomDAO
import com.project.wematch.R
import com.project.wematch.TO.GamerTO
import com.project.wematch.TO.RoomDetailTO
import com.project.wematch.Util.SharedPrefsUtils
import rx.Observable
import rx.Subscriber

class GamerListActivity : AppCompatActivity() {

    val rvGamer: RecyclerView by lazy { findViewById<RecyclerView>(R.id.rv_gamer) }
    private val adapter: GamerListAdapter by lazy { GamerListAdapter(this) }

    private var subscriber: Subscriber<List<GamerTO>>? = null

    var gamerId: String? = null
    var gamerName: String? = null
    var mode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gamer_list)

        gamerId = intent.getStringExtra("gamerId")
        gamerName = intent.getStringExtra("gamerName")
        mode = intent.getStringExtra("mode")

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setTitle(gamerName)

        initView()
        getList()
    }

    fun initView() {
        rvGamer.layoutManager = LinearLayoutManager(this)
        rvGamer.adapter = adapter
    }

    fun getList() {
        if (mode == "0") {
            getFollower()
        } else {
            getUserFollowed()
        }
    }

    fun getFollower() {
        val listingTask: Observable<List<GamerTO>>? = GamerDAO(this).showFollowers(gamerId!!)
        subscriber = object : Subscriber<List<GamerTO>>() {
            override fun onError(e: Throwable?) {
            }

            override fun onCompleted() {
            }

            override fun onNext(result: List<GamerTO>?) {
                if (result != null) {
                    adapter.setResult(result, 0)
                }
            }

        }

        if (listingTask != null) {
            listingTask.subscribe(subscriber)
        }
    }

    fun getUserFollowed() {
        val listingTask: Observable<List<GamerTO>>? = GamerDAO(this).showUserFollowed(gamerId!!)
        subscriber = object : Subscriber<List<GamerTO>>() {
            override fun onError(e: Throwable?) {
            }

            override fun onCompleted() {
            }

            override fun onNext(result: List<GamerTO>?) {
                if (result != null) {
                    adapter.setResult(result, 0)
                }
            }

        }

        if (listingTask != null) {
            listingTask.subscribe(subscriber)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
