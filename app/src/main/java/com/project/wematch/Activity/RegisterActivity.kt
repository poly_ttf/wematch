package com.project.wematch.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import com.google.gson.Gson
import com.project.wematch.DAO.GamerDAO
import com.project.wematch.R
import com.project.wematch.TO.GamerTO
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.StringUtils
import com.project.wematch.Util.ViewUtils
import kotlinx.android.synthetic.main.activity_register.*
import rx.Observable
import rx.Subscriber


class RegisterActivity : AppCompatActivity() {

    val etUsername: EditText by lazy { findViewById<EditText>(R.id.et_username) }
    val etEmail: EditText by lazy { findViewById<EditText>(R.id.et_email) }
    val etPassword: EditText by lazy { findViewById<EditText>(R.id.et_password) }
    val etRetypePassword: EditText by lazy { findViewById<EditText>(R.id.et_retype_password) }

    private var subscriber: Subscriber<GamerTO>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initView()
    }

    fun initView() {
        etUsername.setOnFocusChangeListener { view, hasFocus -> if (!hasFocus) isValidUsername() }
        etEmail.setOnFocusChangeListener { view, hasFocus -> if (!hasFocus) isValidEmail() }
        etPassword.setOnFocusChangeListener { view, hasFocus -> if (!hasFocus) isValidPassword() }
        etRetypePassword.setOnFocusChangeListener { view, hasFocus -> if (!hasFocus) isValidRetypePassword() }
    }

    private fun isValidUsername(): Boolean {
        if (TextUtils.isEmpty(etUsername.text.toString())) {
            etUsername.error = getString(R.string.error_empty_text)
            return false
        }
        return true
    }

    private fun isValidEmail(): Boolean {
        if (TextUtils.isEmpty(etEmail.text.toString())) {
            etEmail.error = getString(R.string.error_empty_text)
            return false
        } else if (!StringUtils.isValidEmail(etEmail.text.toString())) {
            etEmail.error = getString(R.string.error_invalid_email)
            return false
        }
        return true
    }

    private fun isValidPassword(): Boolean {
        if (TextUtils.isEmpty(etPassword.text.toString())) {
            etPassword.error = getString(R.string.error_empty_text)
            return false
        } else if (!StringUtils.isValidPassword(etPassword.text.toString())) {
            etPassword.error = getString(R.string.error_invalid_password)
            return false
        }
        return true
    }

    private fun isValidRetypePassword(): Boolean {
        if (!etRetypePassword.text.toString().equals(etRetypePassword.text.toString())) {
            etRetypePassword.error = getString(R.string.error_wrong_retypepassword)
            return false
        }
        return true
    }

    fun register(v: View) {
        if (isValidUsername() && isValidEmail() && isValidPassword() && isValidRetypePassword()) {
            doRegister()
        }
    }

    private fun doRegister() {
        val gamer = GamerTO()
        gamer.username = etUsername.text.toString()
        gamer.email = etEmail.text.toString()
        gamer.deviceId = SharedPrefsUtils.getStringPreference(this, "pushId")
        gamer.password = etPassword.text.toString()

        val registerTask: Observable<GamerTO>? = GamerDAO(this).createGamer(gamer)
        subscriber = object : Subscriber<GamerTO>() {
            override fun onNext(t: GamerTO?) {
                if (t != null) {
                    SharedPrefsUtils.setStringPreference(this@RegisterActivity, "user", Gson().toJson(t))
                }
            }

            override fun onCompleted() {
                var intent: Intent? = Intent(this@RegisterActivity, MainActivity::class.java)
                startActivity(intent)
            }

            override fun onError(t: Throwable?) {
            }

        }

        registerTask?.subscribe(subscriber)
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP)
            if (!ViewUtils.isPointInsideView(ev.rawX, ev.rawY, etUsername) && !ViewUtils.isPointInsideView(ev.rawX, ev.rawY, et_email) && !ViewUtils.isPointInsideView(ev.rawX, ev.rawY, et_password) && !ViewUtils.isPointInsideView(ev.rawX, ev.rawY, et_retype_password)) {
                ViewUtils.hideKeyboard(this)
            }
        return super.dispatchTouchEvent(ev)
    }

}
