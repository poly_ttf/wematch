package com.project.wematch.Activity

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.project.wematch.DAO.RoomDAO
import com.project.wematch.R
import com.project.wematch.TO.*
import com.project.wematch.Util.SharedPrefsUtils
import com.project.wematch.Util.StringUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_room_detail.*
import rx.Observable
import rx.Subscriber

class RoomDetailActivity : AppCompatActivity() {

    var room: RoomDetailTO = RoomDetailTO()

    val ivRoomPic: ImageView by lazy { findViewById<ImageView>(R.id.iv_game_pic) }
    val tvRoomName: TextView by lazy { findViewById<TextView>(R.id.tv_room_name) }
    val tvCreateAt: TextView by lazy { findViewById<TextView>(R.id.tv_create_at) }
    val tvGameDate: TextView by lazy { findViewById<TextView>(R.id.tv_game_date) }
    val tvRoomCaption: TextView by lazy { findViewById<TextView>(R.id.tv_room_caption) }

    val btnJoinRoom: TextView by lazy { findViewById<Button>(R.id.btn_join_room) }

    private var roomSubscriber: Subscriber<List<RoomDetailTO>>? = null
    private var joinRoomSubscriber: Subscriber<ActionResultTO>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_detail)

        if (intent.extras != null && intent.extras.containsKey("room_id")) {
            room.room_id = intent.extras.getString("room_id")
        } else {
            room.room_id = intent.getStringExtra("roomId")
        }

        initView()
        getRoomDetail()
    }

    private fun initView() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        btnJoinRoom.setOnClickListener { view -> joinRoom() }
    }

    private fun getRoomDetail() {
        val roomDetailTask: Observable<List<RoomDetailTO>>? = RoomDAO(this).getRoomDetail(room.room_id!!, SharedPrefsUtils.getMyId(this)!!)
        roomSubscriber = object : Subscriber<List<RoomDetailTO>>() {
            override fun onNext(t: List<RoomDetailTO>?) {
                if (t != null) {
                    room = t[0]
                    updateUI()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }
        }

        roomDetailTask?.subscribe(roomSubscriber)

    }

    private fun joinRoom() {
        val joinRoomTask: Observable<ActionResultTO>? = RoomDAO(this).joinRoom(JoinRoomTO(SharedPrefsUtils.getMyId(this), room.room_id))
        joinRoomSubscriber = object : Subscriber<ActionResultTO>() {
            override fun onNext(result: ActionResultTO?) {
                if (result != null) {
                    if (result.isSuccess) {
                        val messageDialog = AlertDialog.Builder(this@RoomDetailActivity)
                        messageDialog.setTitle(getString(R.string.congratulation))
                        messageDialog.setMessage(getString(R.string.message_success_join_room))
                        messageDialog.setPositiveButton(getString(R.string.confirm), { dialog, which -> Unit })
                        messageDialog.setCancelable(false)
                        messageDialog.show()
                    } else {
                        val messageDialog = AlertDialog.Builder(this@RoomDetailActivity)
                        messageDialog.setTitle(getString(R.string.error))
                        messageDialog.setCancelable(false)
                        messageDialog.setMessage(getString(R.string.message_fail_join_room))
                        messageDialog.setPositiveButton(getString(R.string.confirm), { dialog, which -> Unit })
                        messageDialog.show()
                    }
                    getRoomDetail()
                }
            }

            override fun onCompleted() {
            }

            override fun onError(t: Throwable?) {
            }
        }

        joinRoomTask?.subscribe(joinRoomSubscriber)
    }

    private fun updateUI() {
        tv_username.text = room.username
        tvRoomName.text = room.room_name
        tvCreateAt.text = StringUtils.dayDiff(this, room.created_at!!)
        tvGameDate.text = room.game_date + " " + room.game_time
        tvRoomCaption.text = room.caption
        tv_joint_count.text = room.total_join
        Picasso.with(this).load(room.master_image).placeholder(R.drawable.ic_loading).into(ivRoomPic)
        Picasso.with(this).load(room.profile_pic).placeholder(R.drawable.ic_launcher).into(iv_user_pic)

        if (room.is_join) {
            btnJoinRoom.text = "Your joint this room"
            btnJoinRoom.setBackgroundColor(ContextCompat.getColor(this, R.color.grey))
            btnJoinRoom.isEnabled = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
