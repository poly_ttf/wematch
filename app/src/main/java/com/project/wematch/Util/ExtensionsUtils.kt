package com.project.wematch.Util

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.project.wematch.R
import java.io.Serializable

/**
 * Created by fish on 20/11/2017.
 */

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun AppCompatActivity.swapFragment(fragment: Fragment?) {
    supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
}

fun AppCompatActivity.swapFragment(fragment: Fragment?, name: String) {
    supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.right_enter, R.anim.left_exit, R.anim.left_enter, R.anim.right_exit)
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(name)
            .commit()

    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setHomeButtonEnabled(true)
}

fun <T : Serializable> Bundle.getSerializable(key: String): Serializable? {
    if (containsKey(key)) {
        return getSerializable(key) as T
    } else {
        return null
    }
}
