package com.project.wematch.Util

import android.content.Context
import android.text.TextUtils
import com.project.wematch.Activity.MainActivity
import com.project.wematch.R
import kotlinx.android.synthetic.main.activity_main.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Fish on 2017/11/22.
 */

object StringUtils {
    fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isValidPassword(password: String): Boolean {
        return password.length >= 6
    }

    fun dayDiff(ctx: Context, date: String): String {
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd kk:mm", Locale.CHINESE)
            val cal_date = strToCalendar(date, formatter)
            val cal_current = Calendar.getInstance()

            val diff = cal_current.timeInMillis / 60000 - cal_date.timeInMillis / 60000
            return if (diff < 1) {
                //1分鐘內
                ctx.getString(R.string.time_now)
            } else if (diff in 1..59) {
                //1小時內
                diff.toString() + ctx.getString(R.string.time_minute)
            } else if (diff in 60..1439) {
                //1天內
                (diff / 60).toString() + ctx.getString(R.string.time_hour)
            } else if (cal_current.get(Calendar.YEAR) - cal_date.get(Calendar.YEAR) === 0) {
                //1年內
                SimpleDateFormat(ctx.getString(R.string.date_format_pattern_within_a_year), Locale.CHINESE).format(formatter.parse(date))
            } else {
                //1年後
                SimpleDateFormat(ctx.getString(R.string.date_format_pattern_over_a_year), Locale.CHINESE).format(formatter.parse(date))
            }
        } catch (e: ParseException) {
            return date
        }

    }

    fun strToDate(date: String, formatter: SimpleDateFormat): Date? {
        try {
            return formatter.parse(date)
        } catch (e: ParseException) {
            return null
        }

    }

    fun dateToCalendar(date: Date?): Calendar {
        val cal_date = Calendar.getInstance()
        cal_date.time = date
        return cal_date
    }

    fun strToCalendar(date: String, formatter: SimpleDateFormat): Calendar {
        return dateToCalendar(strToDate(date, formatter))
    }

    fun setTitle(activity: MainActivity) {
        when (activity.tl_main.selectedTabPosition) {
            0 -> activity.supportActionBar?.title = activity.getString(R.string.app_name)
            1 -> activity.supportActionBar?.title = "Create Room"
            2 -> activity.supportActionBar?.title = "Search  Room"
            3 -> activity.supportActionBar?.title = "User Info"
        }
    }

}