package com.project.wematch.Util

import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.view.View
import android.content.Context.INPUT_METHOD_SERVICE
import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.view.WindowManager
import android.util.DisplayMetrics


/**
 * Created by Fish on 2017/11/22.
 */
object ViewUtils {

    fun isPointInsideView(x: Float, y: Float, view: View): Boolean {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        val viewX = location[0]
        val viewY = location[1]

        return x > viewX && x < viewX + view.getWidth() && y > viewY && y < viewY + view.getHeight()
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
    }

    fun getDisplayMetrics(ctx: Context): DisplayMetrics {
        val metrics = DisplayMetrics()
        val wm = ctx.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(metrics)
        return metrics
    }

}